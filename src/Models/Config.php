<?php

namespace Schenley\Config\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Part of the Modules package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Modules
 * @version    1.0.0
 * @author     Schenley Learning
 * @license    MIT License
 * @copyright  (c) 2015, Schenley Learning, LLC
 */

class Config extends Model
{
	/**
	 * {@inheritDoc}
	 */
	protected $table = 'config';

	/**
	 * {@inheritDoc}
	 */
	protected $fillable = ['item', 'value'];

	/**
	 * {@inheritDoc}
	 */
	protected $casts = ['value' => 'array'];
}
