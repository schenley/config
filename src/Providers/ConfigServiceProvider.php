<?php

namespace Schenley\Config\Providers;

use PDOException;
use Schenley\Config\Repository;
use Schenley\Support\ServiceProvider;

/**
 * Part of the Modules package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Modules
 * @version    1.0.0
 * @author     Schenley Learning
 * @license    MIT License
 * @copyright  (c) 2015, Schenley Learning, LLC
 */

class ConfigServiceProvider extends ServiceProvider
{

	/**
	 * {@inheritDoc}
	 */
	public function boot()
	{
		$this->setupResources();
		$this->setupRepository();
        $this->registerSettingBladeWidget();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->overrideConfigInstance();
	}

	/**
	 * Overrides the config instance.
	 *
	 * @return void
	 */
	protected function overrideConfigInstance()
	{
		$this->app->register('Illuminate\Cache\CacheServiceProvider');

		$repository = new Repository([], $this->app['cache']);

		$old = $this->app['config']->all();

		foreach ($old as $key => $value) {
			$repository->set($key, $value);
		}

		$this->app->instance('config', $repository);
	}


	/**
	 * Sets up the custom config repository
	 */
	protected function setupRepository()
	{
		$config = $this->app['config'];

		try {
			$config->setCacheKey(config('schenley-config.cache_key'));
			$config->setModel(config('schenley-config.model'));
			$config->setCached();
		} catch (PDOException $e) {}
	}


	/**
	 * Sets up the packages resources
	 */
	protected function setupResources()
	{
		// Publish config
		$config = realpath(__DIR__ . '/../../config/config.php');

		$this->mergeConfigFrom($config, 'schenley-config');

		$this->publishes([
			$config => config_path('schenley-config.php'),
		], 'config');

		// Publish migrations
		$migrations = realpath(__DIR__ . '/../../database/migrations');

		$this->publishes([
			$migrations => $this->app->databasePath().'/migrations',
		], 'migrations');
	}

    /**
     * Register the Blade @setting extension.
     *
     * @return void
     */
    protected function registerSettingBladeWidget()
    {
        $this->app['blade.compiler']->directive('setting', function ($value) {
            return "<?php echo app('config')->get$value; ?>";
        });
    }
}
