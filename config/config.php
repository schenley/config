<?php

/**
 * Part of the Config package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Config
 * @version    1.0.0
 * @author     Schenley Learning
 * @license    MIT License
 * @copyright  (c) 2015, Schenley Learning, LLC
 */

return [

	/*
    |--------------------------------------------------------------------------
    | Cache Key
    |--------------------------------------------------------------------------
    |
    | Specify the name of the key you want to use when caching config items.
    |
    */
	'cache_key' => 'schenley.config',

	/*
    |--------------------------------------------------------------------------
    | Model
    |--------------------------------------------------------------------------
    |
    | Specify the name of the model to use
    |
    */
	'model' => 'Schenley\Config\Models\Config'
];
